import java.util.Scanner;
public class ApplianceStore{
	
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		AirFryer[] fryers = new AirFryer[4];
		
		for(int i = 0; i < 4; i++){
			fryers[i] = new AirFryer();
			System.out.println("input air fryer brand");
			fryers[i].brand = scan.next();
			System.out.println("input air fryer maximum temperature (of your prefered scale)");
			fryers[i].maxTemp = scan.nextInt();
			System.out.println("input air fryer internal volume (litres)");
			fryers[i].volumeLitres = scan.nextDouble();
		}
		
		fryers[3].printDetails();
		
		fryers[0].brag();
		System.out.println("input the needed temperature for a recpie, and then the volume in litres");
		fryers[0].canCook(scan.nextInt(), scan.nextDouble());
		
	}
	
}