public class AirFryer{
	
	public int maxTemp;
	public double volumeLitres;
	public String brand;
	
	public void canCook(int recipeTemp, double recipeVol){
		if (this.maxTemp>= recipeTemp && this.volumeLitres >= recipeVol){
			System.out.println("This recipe can be cooked in this air fryer!");
		}
		else{
			System.out.println("this recipie is incompatible with this air fryer");
		}
	}
	
	public void brag(){
		System.out.println("i as a "+brand+" brand airfryer am far superior to any other airfryer in the buisness.");
		System.out.println("my internal volume of "+volumeLitres+" is the perfect size, greater then all else");
		System.out.println("and my maximum temperature of "+maxTemp+" could scorch your foes to dust, or cook food requiring high temperatures. that aswell.");
	}
	
	public void printDetails(){
		System.out.println(brand);
		System.out.println("Max temperature of "+maxTemp+" mystery degrees");
		System.out.println("Internal volume of "+volumeLitres+" litres");
	}
}